package fr.epsi.b3.cocodom;

import android.app.Application;

public class CocoDom extends Application {

    private String title;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
