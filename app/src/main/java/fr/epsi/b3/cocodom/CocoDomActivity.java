package fr.epsi.b3.cocodom;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class CocoDomActivity extends Activity implements View.OnClickListener {
    protected CocoDom cocoDom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cocoDom = (CocoDom) getApplication();
    }

    protected void setTitle(String title){
        TextView textViewTitle = findViewById(R.id.textViewHeaderTitle);
        if(textViewTitle != null){
            textViewTitle.setText(title);
        }
    }

    protected void deconnexionButton(){
        ImageView imageView = findViewById(R.id.imageViewHeaderDeco);
        if(imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageViewHeaderDeco:
                finish();
                break;
        }
    }

    protected void displayToast(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }
}
