package fr.epsi.b3.cocodom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.text.Normalizer;
import java.util.List;

import fr.epsi.b3.cocodom.model.Capteur;

public class CapteurAdapter extends ArrayAdapter<Capteur> {

    public CapteurAdapter(@NonNull Context context, int resource, @NonNull List<Capteur> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View capteurView, @NonNull ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        capteurView = layoutInflater.inflate(R.layout.c_capteur, null);

        TextView textViewCapteurType = capteurView.findViewById(R.id.textViewCapteur);
        TextView textViewCapteurValue = capteurView.findViewById(R.id.textViewValue);
        TextView textViewCapteurUnit = capteurView.findViewById(R.id.textViewUnit);
        ImageView imageViewCapteur = capteurView.findViewById(R.id.imageViewCapteur);

        Capteur capteur = getItem(position);

        textViewCapteurType.setText(capteur.getType());
        textViewCapteurValue.setText(Double.toString(capteur.getValue()));
        textViewCapteurUnit.setText(capteur.getUnit());
//        String capteurType = Normalizer.normalize(capteur.getType().toLowerCase(), Normalizer.Form.NFD).replaceAll("[^\p{ASCII}]", "");
        switch (capteur.getType()){
            case "Température":
                Picasso.get().load(R.drawable.themometer_transparent).into(imageViewCapteur);
                break;
            case  "Humidité":
                Picasso.get().load(R.drawable.humidity_transparent).into(imageViewCapteur);
                break;
            default:
                break;
        }
        return capteurView;
    }
}
