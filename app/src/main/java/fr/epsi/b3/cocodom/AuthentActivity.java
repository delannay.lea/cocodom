package fr.epsi.b3.cocodom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AuthentActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_authent);
        super.onCreate(savedInstanceState);
        Button log = findViewById(R.id.loginButton);
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO : remettre cette ligne avant de push
                AuthentActivity.this.login(v);
            }
        });
    }

    private void login(View v) {
        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = this.checkCredentials(username.getText(), password.getText());
        queue.add(stringRequest);
    }

    private StringRequest checkCredentials(final Editable username, final Editable password) {
        String loginUrl = "http://192.168.1.20:8001/login";
        StringRequest request = new StringRequest(Request.Method.POST, loginUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("Connexion réussie");
                            Intent intent = new Intent(AuthentActivity.this, MainActivity.class);
//                            JSONObject client = new JSONObject(response);
                            intent.putExtra("idClient", "1");
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
                /*System.out.println(error.networkResponse.statusCode);*/
                Toast.makeText(AuthentActivity.this, "La connexion a échoué.", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                String emailParam = username.toString();
                String passwordParam = password.toString();
                params.put("username", emailParam);
                params.put("password", passwordParam);
                return params;
            }
        };
        return request;
    }
}
