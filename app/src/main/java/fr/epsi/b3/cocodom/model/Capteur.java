package fr.epsi.b3.cocodom.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Capteur {
    private String type;
    private String unit;
    private double value;

    public Capteur(JSONObject jsonObject) throws JSONException {
        this.type = jsonObject.optString("type","");
        this.unit = jsonObject.optString("unit","");
        this.value = jsonObject.getDouble("value");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
