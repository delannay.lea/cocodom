package fr.epsi.b3.cocodom;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.epsi.b3.cocodom.model.Capteur;

public class MainActivity extends CocoDomActivity {

    private ArrayAdapter<Capteur> arrayAdapter;
    private ArrayList<Capteur> capteurs;
    private String urlApi = "http://192.168.1.20:8001/api/capteurs";
    //TODO: supprimer les 2 lignes quand API OK
    //JSONArray jsonArray = new JSONArray();
    //JSONObject capteursObj = new JSONObject();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        deconnexionButton();
        super.onCreate(savedInstanceState);
        setTitle("Dashboard");

        capteurs = new ArrayList<>();
        ListView listViewCapteurs = findViewById(R.id.listViewCapteurs);
        arrayAdapter = new CapteurAdapter(this, R.layout.c_capteur, capteurs);
        listViewCapteurs.setAdapter(arrayAdapter);

        //TODO: SUPPRIMER QUAND API OK
        //initListeCapteurs("");

        //TODO: REMETTRE QUAND API OK
        new HttpService(urlApi, new HttpService.HttpServiceListener() {
            @Override
            public void apiDone(String result) {
                initListeCapteurs(result);
            }

            @Override
            public void apiError(Exception e) {
                displayToast(e.getMessage());
            }
        }).execute();
    }

    private void initListeCapteurs(String data){
        try {
            //TODO: REMETTRE QUAND API OK
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("capteurs");
            //TODO: SUPPRIMER QUAND API OK
            //createJsonObjectTemp();
            for(int i = 0; i<jsonArray.length(); i++){
                Capteur capteur = new Capteur(jsonArray.getJSONObject(i));
                capteurs.add(capteur);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        arrayAdapter.notifyDataSetChanged();
    }

    //TODO: SUPPRIMER QUAND API OK
//    public void createJsonObjectTemp() throws JSONException {
//        JSONObject capteur1 = new JSONObject();
//        try {
//            capteur1.put("type", "Temperature");
//            capteur1.put("unit", "°C");
//            capteur1.put("value", "20.0");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JSONObject capteur2 = new JSONObject();
//        try {
//            capteur2.put("type", "Humidity");
//            capteur2.put("unit", "%");
//            capteur2.put("value", "69.0");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        jsonArray.put(capteur1);
//        jsonArray.put(capteur2);
//        capteursObj.put("capteurs", jsonArray);
//    }
}
